#ifndef LIBENERGY
#define LIBENERGY

#include <stdlib.h>
#include "error.h"

#define ENERGY struct energy

ENERGY
{
	char *name;
};

int initEnergy(ENERGY *);
int defineEnergy(ENERGY *, char *);
int endEnergy(ENERGY *);

#endif
