#ifndef LIBENGINE
#define LIBENGINE

#include <stdlib.h>
#include "fuel.h"
#include "error.h"

#define ENGINE struct engine

ENGINE
{
	int id;
	FUEL *fuel;
	float power;
};

int initEngine(ENGINE *);
int defineEngine(ENGINE *, int, FUEL *, float, char);
int endEngine(ENGINE *);

#endif
