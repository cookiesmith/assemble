#include <stdio.h>
#include <stdlib.h>

#include "part.h"
#include "fuel.h"
#include "engine.h"

int main(void)
{
	PART *novaParte = (PART *)malloc(sizeof(PART)); //Parte
	ENGINE *cu = (ENGINE *) malloc(sizeof(ENGINE)); //Unidade Consumidora
	ENGINE *gu = (ENGINE *) malloc(sizeof(ENGINE)); //Unidade Geradora
	FUEL *fuel = (FUEL *)malloc(sizeof(FUEL)); //Combustível das unidades

	initPart(novaParte);
	initEngine(gu);
	initEngine(cu);
	initFuel(fuel);

	char *nome = (char *)malloc(20*sizeof(char));
	scanf(" %s", nome);

	defineFuel(fuel, nome, 10, 11);
	defineEngine(cu, 1, fuel, 10.1, 1);
	defineEngine(gu, 2, fuel, 10.2, 0);
	definePart(novaParte, 3, nome, cu, gu);

	endFuel(fuel, nome, 10, 11);
	endEngine(cu, 1, fuel, 10.1, 1);
	endEngine(gu, 2, fuel, 10.2, 0);
	endPart(novaParte, 3, nome, cu, gu);

	return 0;
}
