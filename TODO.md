# TODO LIST

## GERAL

- [ ] 4: Criar estrutura das peças (drivers, e geradores, e todo o resto);

- [ ] 6: Fazer um programa para automatizar os TODO's de cada source;

- [ ] 7: Fazer um programa para automatizar a criação do manual do game, baseado nos comentários de cada source;

- [ ] 8: Padronizar arquivos de documentação

## CONCEITUAÇÃO

- [ ] 3: Revisar conceitos básicos;

- [ ] 5: Revisar Energias;

- [ ] 7: Revisar Combustíveis;

- [ ] 8: Definir Controladores;

- [ ] 9: Revisar Controladores;

- [ ] 10: Definir Armas e escudos;

- [ ] 11: Revisar Armas e escudos;

- [ ] 12: Definir Sensores;

- [ ] 13: Revisar Sensores;

- [ ] 14: Definir Atuadores;

- [ ] 15: Revisar Atuadores;

## PEÇAS

- [ ] 1: Definir modelos de conversores

## LORE



